package io.personalsoft.testService.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api2")
public class TestController {
	
	private int actSum = 0;
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final String triquiMatrix[][] = new String[3][3];

	
	@GetMapping(value = "obtener_hello", produces = MediaType.APPLICATION_JSON_VALUE)
	public String sayHello() {
		return "HolaS";
	}
	
	@GetMapping(value = "obtener_bye", produces = MediaType.APPLICATION_JSON_VALUE)
	public String sayBye() {
		return "Adios";
	}
	
	@GetMapping(value = "sum", produces = MediaType.APPLICATION_JSON_VALUE)
	public String sum() {
		this.actSum++;
		return "Va en: " + this.actSum;
	}
	
	@GetMapping(value = "try", produces = MediaType.APPLICATION_JSON_VALUE)
	public String tryc(@RequestParam int x, @RequestParam int y) {
        int [][] gato = new int [x][y];
        String res = "";
        // int cont = 0;

        for(int i = 0; i < gato.length; i++) {
            for(int j = 0; j < gato.length; j++) {
                gato[i][j] = (int) (Math.random()*20);
                res = res + String.valueOf(gato[i][j]) + ",";
            }
        }
        return res;
	}
	
	@GetMapping(value = "verify_triqui", produces = MediaType.APPLICATION_JSON_VALUE)
	public String verifyTriqui(@RequestParam int xPosition, @RequestParam int yPosition, @RequestParam boolean type) {
		logger.info(xPosition + "-");
		logger.info(yPosition + "-");
		logger.info(type + "-");
		if (type) {
			this.triquiMatrix[xPosition][yPosition] = "O";
		} else {
			this.triquiMatrix[xPosition][yPosition] = "X";
		}
		return this.hasTriqui(type);
	}
	
	@GetMapping(value = "get_last_matrix", produces = MediaType.APPLICATION_JSON_VALUE)
	public String getLastMatrix() {
		return this.getStringMaxtrix();
	}

	@GetMapping(value = "start_triqui", produces = MediaType.APPLICATION_JSON_VALUE)
	public String startTriqui() {
		this.startMatrix();
		return getStringMaxtrix();
	}

	private String hasTriqui(boolean type) {
		if (type) {
			if (this.triquiMatrix[0][0].equals("O") 
					&& this.triquiMatrix[0][1].equals("O") 
					&& this.triquiMatrix[0][2].equals("O")) {
				return "O-TK";
			}
			if (this.triquiMatrix[1][0].equals("O") 
					&& this.triquiMatrix[1][1].equals("O") 
					&& this.triquiMatrix[1][2].equals("O")) {
				return "O-TK";
			}
			if (this.triquiMatrix[2][0].equals("O") 
					&& this.triquiMatrix[2][1].equals("O") 
					&& this.triquiMatrix[2][2].equals("O")) {
				return "O-TK";
			}
			if (this.triquiMatrix[0][0].equals("O") 
					&& this.triquiMatrix[1][0].equals("O") 
					&& this.triquiMatrix[2][0].equals("O")) {
				return "O-TK";
			}
			if (this.triquiMatrix[0][1].equals("O") 
					&& this.triquiMatrix[1][1].equals("O") 
					&& this.triquiMatrix[2][1].equals("O")) {
				return "O-TK";
			}
			if (this.triquiMatrix[0][2].equals("O") 
					&& this.triquiMatrix[1][2].equals("O") 
					&& this.triquiMatrix[2][2].equals("O")) {
				return "O-TK";
			}
			if (this.triquiMatrix[0][0].equals("O") 
					&& this.triquiMatrix[1][1].equals("O") 
					&& this.triquiMatrix[2][2].equals("O")) {
				return "O-TK";
			}
			if (this.triquiMatrix[2][0].equals("O") 
					&& this.triquiMatrix[1][1].equals("O") 
					&& this.triquiMatrix[0][2].equals("O")) {
				return "O-TK";
			}
		} else {
			if (this.triquiMatrix[0][0].equals("X") 
					&& this.triquiMatrix[0][1].equals("X") 
					&& this.triquiMatrix[0][2].equals("X")) {
				return "X-TK";
			}
			if (this.triquiMatrix[1][0].equals("X") 
					&& this.triquiMatrix[1][1].equals("X") 
					&& this.triquiMatrix[1][2].equals("X")) {
				return "X-TK";
			}
			if (this.triquiMatrix[2][0].equals("X") 
					&& this.triquiMatrix[2][1].equals("X") 
					&& this.triquiMatrix[2][2].equals("X")) {
				return "X-TK";
			}
			if (this.triquiMatrix[0][0].equals("X") 
					&& this.triquiMatrix[1][0].equals("X") 
					&& this.triquiMatrix[2][0].equals("X")) {
				return "X-TK";
			}
			if (this.triquiMatrix[0][1].equals("X") 
					&& this.triquiMatrix[1][1].equals("X") 
					&& this.triquiMatrix[2][1].equals("X")) {
				return "X-TK";
			}
			if (this.triquiMatrix[0][2].equals("X") 
					&& this.triquiMatrix[1][2].equals("X") 
					&& this.triquiMatrix[2][2].equals("X")) {
				return "X-TK";
			}
			if (this.triquiMatrix[0][0].equals("X") 
					&& this.triquiMatrix[1][1].equals("X") 
					&& this.triquiMatrix[2][2].equals("X")) {
				return "X-TK";
			}
			if (this.triquiMatrix[2][0].equals("X") 
					&& this.triquiMatrix[1][1].equals("X") 
					&& this.triquiMatrix[0][2].equals("X")) {
				return "X-TK";
			}
		}
		return getStringMaxtrix();
	}

	private String getStringMaxtrix() {
		String stringMatrix = "";
		for (int i = 0; i < this.triquiMatrix.length; i++) {
			for (int j = 0; j < this.triquiMatrix[i].length; j++) {
				stringMatrix += this.triquiMatrix[i][j] + ",";
			}
		}
		return (stringMatrix.substring(0, stringMatrix.length() - 1));
	}

	private void startMatrix() {
		this.triquiMatrix[0][0] = "-";
		this.triquiMatrix[0][1] = "-";
		this.triquiMatrix[0][2] = "-";
		this.triquiMatrix[1][0] = "-";
		this.triquiMatrix[1][1] = "-";
		this.triquiMatrix[1][2] = "-";
		this.triquiMatrix[2][0] = "-";
		this.triquiMatrix[2][1] = "-";
		this.triquiMatrix[2][2] = "-";
	}
}
